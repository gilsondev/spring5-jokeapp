package guru.springframework.joke.services;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JokeServiceTest {

    @Autowired
    private JokeService jokeService;

    @MockBean
    private ChuckNorrisQuotes chuckNorrisQuotes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnRandomJoke() {
        when(chuckNorrisQuotes.getRandomQuote()).thenReturn("A Chuck Norris Joke Random");

        String result = jokeService.getJoke();

        Assert.assertEquals("A Chuck Norris Joke Random", result);
    }
}
