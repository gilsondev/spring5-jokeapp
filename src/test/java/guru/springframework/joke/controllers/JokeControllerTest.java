package guru.springframework.joke.controllers;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JokeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ChuckNorrisQuotes chuckNorrisQuotes;

    @Test
    public void shouldViewChuckNorrisJoke() throws Exception {
        when(chuckNorrisQuotes.getRandomQuote()).thenReturn("A Chuck Norris Joke Random");

        this.mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("A Chuck Norris Joke Random")));
    }
}
